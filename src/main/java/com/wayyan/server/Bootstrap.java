package com.wayyan.server;

import com.wayyan.server.spark.voter.DeleteVoterRoute;
import com.wayyan.server.spark.voter.LoginVoterRoute;
import com.wayyan.server.spark.voter.PostVoterRoute;
import spark.Spark;

public class Bootstrap {
    public static void main(String[] args) {
        Spark.setPort(getHerokuAssignedPort());
        Spark.get(new LoginVoterRoute("/voters/:username/password/:pwd"));
        Spark.post(new PostVoterRoute("/voters"));
        Spark.delete(new DeleteVoterRoute("/voters/:id"));
    }

    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567;
    }
}
