package com.wayyan.server.spark.voter;

import com.avaje.ebean.Ebean;
import com.wayyan.server.JsonTransformer;
import com.wayyan.server.domain.Voter;
import spark.Request;
import spark.Response;

/**
 * Created by Way Yan on 12/18/2015.
 */

public class LoginVoterRoute extends JsonTransformer {
    public LoginVoterRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        String voterUsername = request.params(":username");
        String voterPassword = request.params(":pwd");
        Voter voter = Ebean.find(Voter.class).where().eq("username", voterUsername).findUnique();
        if (voter != null) {
            if (voter.getPassword().equals(voterPassword)) {
                return voter;
            }
            else {
                response.status(404); // 404 Not found
                return createErrorResponse("Wrong password");
            }
        }
        else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
}
