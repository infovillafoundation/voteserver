package com.wayyan.server.spark.voter;

import com.avaje.ebean.Ebean;
import com.wayyan.server.JsonTransformer;
import com.wayyan.server.domain.Voter;
import spark.Request;
import spark.Response;

import java.io.IOException;

/**
 * Created by Way Yan on 12/18/2015.
 */

public class PostVoterRoute extends JsonTransformer {
    public PostVoterRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        Voter voter = null;
        try {
            voter = mapper.readValue(request.body(), Voter.class);
        } catch (IOException e) {

            response.status(500);

            return createErrorResponse("Booking couldn't be saved");
        }
        Ebean.save(voter);
        response.status(201); // 201 Created
        return voter;
    }
}
