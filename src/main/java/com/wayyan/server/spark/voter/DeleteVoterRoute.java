package com.wayyan.server.spark.voter;

import com.avaje.ebean.Ebean;
import com.wayyan.server.JsonTransformer;
import com.wayyan.server.domain.Voter;
import spark.Request;
import spark.Response;

/**
 * Created by Way Yan on 12/18/2015.
 */

public class DeleteVoterRoute extends JsonTransformer{
    public DeleteVoterRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        long voterId = Long.parseLong(request.params(":id"));
        Voter voter = Ebean.find(Voter.class, voterId);
        if (voter != null) {
            Ebean.delete(voter);
            response.status(204); // 204 No Content
            return "";
        } else {
            response.status(404); // 404 Not found
            return createErrorResponse("Not found");
        }
    }
}
