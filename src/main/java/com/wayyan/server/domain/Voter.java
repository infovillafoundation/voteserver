package com.wayyan.server.domain;

import javax.persistence.Column;

/**
 * Created by Way Yan on 12/18/2015.
 */
public class Voter {
    @Column(length = 20, nullable = false, unique = true)
    private String username;
    @Column(length = 20, nullable = false, unique = true)
    private String password;

    public Voter() {

    }

    public Voter(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
